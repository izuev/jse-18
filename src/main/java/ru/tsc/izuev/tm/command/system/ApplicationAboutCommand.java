package ru.tsc.izuev.tm.command.system;

public class ApplicationAboutCommand extends AbstractSystemCommand {
    private static final String NAME = "about";

    private static final String ARGUMENT = "-a";

    private static final String DESCRIPTION = "Show developer info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Igor Zuev");
        System.out.println("E-mail: izuev@t1-consulting.ru");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
