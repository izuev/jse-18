package ru.tsc.izuev.tm.command.user;

import ru.tsc.izuev.tm.api.service.IAuthService;
import ru.tsc.izuev.tm.api.service.IUserService;
import ru.tsc.izuev.tm.command.AbstractCommand;
import ru.tsc.izuev.tm.exception.entity.UserNotFoundException;
import ru.tsc.izuev.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @Override
    public String getArgument() {
        return null;
    }

}
