package ru.tsc.izuev.tm.command.system;

import ru.tsc.izuev.tm.api.service.ICommandService;
import ru.tsc.izuev.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}
