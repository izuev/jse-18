package ru.tsc.izuev.tm.command.system;

import static ru.tsc.izuev.tm.util.FormatUtil.formatBytes;

public class ApplicationInfoCommand extends AbstractSystemCommand {

    public static final String NAME = "info";

    public static final String ARGUMENT = "-i";

    public static final String DESCRIPTION = "Show system info.";

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");

        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("PROCESSORS: " + processorCount);
        System.out.println("MAX MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
