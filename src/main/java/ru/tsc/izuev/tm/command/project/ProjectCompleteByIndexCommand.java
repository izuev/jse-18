package ru.tsc.izuev.tm.command.project;

import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    private static final String NAME = "project-complete-by-index";

    private static final String DESCRIPTION = "Complete project by index.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
