package ru.tsc.izuev.tm.command.user;

import ru.tsc.izuev.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    private static final String NAME = "user-login";

    private static final String DESCRIPTION = "User login.";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
