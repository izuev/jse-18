package ru.tsc.izuev.tm.command.task;

import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    private static final String NAME = "task-change-status-by-id";

    private static final String DESCRIPTION = "Change task status by id.";

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusDisplayName = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusDisplayName);
        getTaskService().changeStatusById(id, status);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
