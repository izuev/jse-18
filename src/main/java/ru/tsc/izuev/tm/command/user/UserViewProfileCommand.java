package ru.tsc.izuev.tm.command.user;

import ru.tsc.izuev.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    private static final String NAME = "user-view-profile";

    private static final String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        showUser(user);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
