package ru.tsc.izuev.tm.command.user;

import ru.tsc.izuev.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    private static final String NAME = "user-update-profile";

    private static final String DESCRIPTION = "Update profile of current user.";

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("[FIRST NAME]");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("[MIDDLE NAME]");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("[LAST NAME]");
        final String lastName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, middleName, lastName);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
