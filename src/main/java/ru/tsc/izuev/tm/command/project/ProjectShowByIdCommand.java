package ru.tsc.izuev.tm.command.project;

import ru.tsc.izuev.tm.model.Project;
import ru.tsc.izuev.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "project-show-by-id";

    private static final String DESCRIPTION = "Show project by id.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findById(id);
        showProject(project);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
