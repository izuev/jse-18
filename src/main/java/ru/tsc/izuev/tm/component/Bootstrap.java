package ru.tsc.izuev.tm.component;

import ru.tsc.izuev.tm.api.repository.ICommandRepository;
import ru.tsc.izuev.tm.api.repository.IProjectRepository;
import ru.tsc.izuev.tm.api.repository.ITaskRepository;
import ru.tsc.izuev.tm.api.repository.IUserRepository;
import ru.tsc.izuev.tm.api.service.*;
import ru.tsc.izuev.tm.command.AbstractCommand;
import ru.tsc.izuev.tm.command.project.*;
import ru.tsc.izuev.tm.command.system.*;
import ru.tsc.izuev.tm.command.task.*;
import ru.tsc.izuev.tm.command.user.*;
import ru.tsc.izuev.tm.enumerated.Role;
import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.izuev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.izuev.tm.repository.CommandRepository;
import ru.tsc.izuev.tm.repository.ProjectRepository;
import ru.tsc.izuev.tm.repository.TaskRepository;
import ru.tsc.izuev.tm.repository.UserRepository;
import ru.tsc.izuev.tm.service.*;
import ru.tsc.izuev.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationVersionCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationInfoCommand());
        registry(new ApplicationHelpCommand());

        registry(new ProjectCreateCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectListCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());

        registry(new TaskCreateCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskListCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProject());
        registry(new TaskClearCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());

        registry(new ApplicationExitCommand());
    }

    private void initDemoData() {
        userService.create("admin","admin", Role.ADMIN);
        userService.create("user", "user","user@user.ru");

        projectService.create("TEST PROJECT 0", "TEST PROJECT 0");
        projectService.create("TEST PROJECT 1", "TEST PROJECT 1");
        projectService.create("TEST PROJECT 3", "TEST PROJECT 3");
        projectService.create("TEST PROJECT 2", "TEST PROJECT 2");
        projectService.changeStatusByIndex(0, Status.IN_PROGRESS);
        projectService.changeStatusByIndex(2, Status.IN_PROGRESS);
        projectService.changeStatusByIndex(3, Status.COMPLETED);

        taskService.create("TEST TASK 3", "TEST TASK 3");
        taskService.create("TEST TASK 2", "TEST TASK 2");
        taskService.create("TEST TASK 1", "TEST TASK 1");
        taskService.create("TEST TASK 0", "TEST TASK 0");
        taskService.changeStatusByIndex(0, Status.COMPLETED);
        taskService.changeStatusByIndex(1, Status.IN_PROGRESS);
        taskService.changeStatusByIndex(2, Status.COMPLETED);
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("*** TASK MANAGER IS SHUTTING DOWN ***");
            }
        });
    }

    private void processCommands() {
        initDemoData();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void exit() {
        System.exit(0);
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(final String... args) {
        processArguments(args);
        initLogger();
        processCommands();
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
