package ru.tsc.izuev.tm.api.service;

import ru.tsc.izuev.tm.api.repository.IProjectRepository;
import ru.tsc.izuev.tm.enumerated.Sort;
import ru.tsc.izuev.tm.enumerated.Status;
import ru.tsc.izuev.tm.model.Project;

import java.util.List;

public interface IProjectService extends IProjectRepository {

    List<Project> findAll(Sort sort);

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

}
