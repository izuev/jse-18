package ru.tsc.izuev.tm.api.service;

import ru.tsc.izuev.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String projectId, String taskId);

    void remoteProjectById(String projectId);

    Task unbindTaskFromProject(String projectId, String taskId);

}
