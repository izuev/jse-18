package ru.tsc.izuev.tm.exception.field;

public final class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}
