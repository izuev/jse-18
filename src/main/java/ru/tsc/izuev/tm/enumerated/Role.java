package ru.tsc.izuev.tm.enumerated;

public enum Role {

    ADMIN("Administrator"),
    USUAL("Usual user");

    private final String displayName;


    Role(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
